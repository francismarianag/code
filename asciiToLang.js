

	function asciiConvert (str, lang){
			var arr1 = [];
			for (var n = 0; n < str.length; n ++){
				var dec = (Number(str.charCodeAt(n)+128).toString(16));
				dec = lang[dec.toUpperCase()]
				var conver = String.fromCharCode("0x"+dec)
				arr1.push(conver);
			}
			return arr1.join('');
	}

	function deco (string){
		var array = string.split("(")
		var arr1 = [];
		for (i=0; i<array.length; i++){
			var res = "";
			var langIndicator = array[i].charAt(0);
			switch (langIndicator){
				case "b":
				case "c":
					// Latin 1(West European)
					res = EMV.asciiConvert(array[i].substr(1).trim(), Iso8859.part1);
					break;
				case "d":
				case "e":
					// Latin 2(East European)
					res = EMV.asciiConvert(array[i].substr(1).trim(), Iso8859.part2);
					break;
				case "f":
				case "g":
					// Latin 3(South European)
					res = EMV.asciiConvert(array[i].substr(1).trim(), Iso8859.part3);
					break;
				case "h":
				case "i":
					// Latin 4(North European)
					res = EMV.asciiConvert(array[i].substr(1).trim(), Iso8859.part4);
					break;
				case "j":
				case "k":
					// Latin/Cyrillic
					res = EMV.asciiConvert(array[i].substr(1).trim(), Iso8859.part5);
					break;
				case "l":
				case "m":
					// Latin/Arabic
					res = EMV.asciiConvert(array[i].substr(1).trim(), Iso8859.part6);
					break;
				case "n":
				case "o":
					// Latin/Greek
					res = EMV.asciiConvert(array[i].substr(1).trim(), Iso8859.part7);
					break;
				case "p":
				case "q":
					// Latin/Hebrew
					res = EMV.asciiConvert(array[i].substr(1).trim(), Iso8859.part8);
					break;
				case "r":
				case "s":
					// Turkish
					res = EMV.asciiConvert(array[i].substr(1).trim(), Iso8859.part9);
					break;
				case "t":
				case "u":
					// Nordic
					res = EMV.asciiConvert(array[i].substr(1).trim(), Iso8859.part10);
					break;
				case "`":
				case "a":
					//regreso a la codificacion normal
					res = array[i].substr(1);
					break;
				default :
					res = array[i]
			}
			arr1.push(res)
		}
		return arr1.join(' ');
	},

